%  _____ _      _   _        _         _____   __   __      _   _            _                  
% |_   _| |    | | | |      | |       |_   _|  \ \ / /     | | | |          | |                 
%   | | | |    | |_| |      | |     __ _| | ___ \ V /      | | | | ___  _ __| | __ _  __ _  ___ 
%   | | | |    |  _  |      | |    / _` | |/ _ \/   \      | | | |/ _ \| '__| |/ _` |/ _` |/ _ \
%  _| |_| |____| | | |      | |___| (_| | |  __/ /^\ \     \ \_/ / (_) | |  | | (_| | (_| |  __/
% \___/\_____/\_| |_/      \_____/\__,_\_/\___\/   \/      \___/ \___/|_|  |_|\__,_|\__, |\___|
%                                                                                    __/ |     
%                                                                                   |___/      
% Author:               J. Antes, B. Schoch, J. Acuña, D.Koch 
% Company:              ILH
% LastChangedRevision:  4.0
% LastChangedDate:      06.04.21
%
%    Change Record:
%    Version | Date      | Author        | Section affected
%    -----------------------------------------------------------------------
%    5.0     | 01.09.23  | T. Fink       | Overhaul and modernization 
%	  4.0	    | 06.04.21  | D.Koch		  | Replaced obsolote scrpage2 page
%    3.0     | 17.10.19  | J. Acuña      | Multichapter structure 
%                                        | Fixed 'table' code (\ref didn't work)
%                                        | Added 'good' example to include a png file
%                                        | Added bibliography examples for Bachelor- and Masterarbeit
%                                        | Fixed bug in 'Anhang' code (didn't compile with my Miktex 2.9) 
%                                        | Deleted trans. line drawing (didn't compile with my Miktex 2.9)
%    2.0     | 04.04.17  | B. Schoch     | ? 
%    1.0     | 27.10.14  | J. Antes      | Initial version 


\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{vorlage/ilhthesis_v5}[2023/09/01 v5.0 ILH Thesis]
\PassOptionsToClass{a4paper,12pt,DIV=calc,BCOR=8.25mm,headings=normal}{scrbook}
\LoadClass{scrbook}

\typeout{Latex thesis template, ILH, Uni Stuttgart by Jochen Antes <27 Oct 2014>.
					update by Benjamin Schoch <4 Apr 2017>.
					update by Javier Acuña <June 14th 2019>.
					update by Dominik Koch <April 06th 2021>
               update by Tobias Fink   <September 1st 2023>}

% ILH Vorlage für Master- / Bachelor- / Forschungsarbeiten (Diplom-/ Studien- /)
% Options:  		master			für Masterarbeit
%                     		bachelor		für Bachelorarbeit
%                      		forschung		für Forschungsarbeit
%                      		diplom			für Diplomarbeit
%       				studien  		für Studienarbeit
%
%				
% Befehle:   	\author{Verfasser der Arbeit}
%	               	\betreuer{Betreuer der Arbeit}
%                 	\zweitbetreuer{falls es einen gibt}
%                 	\datestart{01.01.2014}
%                 	\dateend{01.01.2015}
%           		\title{Title der Arbeit}
%
%           		\abstract{TEXT}
%				\abstractsecondlanguage{TEXT}
%           		\maketitle -> macht Deckblatt
%
% 2014.10.24 v1.0 by J. Antes
%
% Changelog
% 
%   24.10.2014		v1.0	Vorlage erstellt 
%		04.04.2017		v2.0 	update
%
\newif\ifg@rman
\DeclareOption{german}{\g@rmantrue}

\newif\if@diplom
\DeclareOption{diplom}{\@diplomtrue}

\newif\if@studien
\DeclareOption{studien}{\@studientrue}

\newif\if@master
\DeclareOption{master}{\@mastertrue}

\newif\if@bachelor
\DeclareOption{bachelor}{\@bachelortrue}

\newif\if@forschung
\DeclareOption{forschung}{\@forschungtrue}

\newif\if@utf
\DeclareOption{utf}{\@utftrue}


\newif\if@zweibetreuer
\@zweibetreuerfalse

% Execute Options
\ProcessOptions 

\addtokomafont{sectioning}{\sffamily} % Setzt in der verwendeten KOMA-Klasse scrbook die Überschriften mit einer Serifenlosen Schrift

\PassOptionsToPackage{top=2.8cm,bottom=3.5cm,inner=2.2cm,outer=2.2cm}
{geometry}  % Passt die Seitenränder an

%=========================================================================================
% --- Standardpakete --- %

\RequirePackage{geometry}
\RequirePackage[headsepline]{scrlayer-scrpage}
\RequirePackage{fontspec}
%\RequirePackage[utf8]{luainputenc}
\if@utf
%\RequirePackage[utf8x]{inputenc}
\else
%\RequirePackage[latin1]{inputenc}
\fi
\RequirePackage{textcomp}					% fuer Symbole/Sonderzeichen (Bsp. Euro)
\RequirePackage{lmodern}					% Ersetzt die Std Schriftart Computer Modern mit Latin Modern (sieht prinzipiell gleich aus, nur nicht so verpixelt)
\RequirePackage{mathtools}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{graphicx}




% Allgemeine Deklarationen
\newcommand{\betreuer}[1]{\def\@betreuer{#1}}   % Betreuer der Arbeit
\def\@betreuer{\relax}
\newcommand{\foreigntitle}[1]{\def\@foreigntitle{#1}}   % Betreuer der Arbeit
\def\@foreigntitle{\relax}
\newcommand{\placeofbirth}[1]{\def\@placeofbirth{#1}}   % Geburtsort
\def\@^placeofbirth{\relax}
\newcommand{\datestart}[1]{\def\@datestart{#1}} % Startdatum der  Arbeit
\def\@datestart{\relax}
\newcommand{\dateend}[1]{\def\@dateend{#1}}     % Enddatum der  Arbeit
\def\@dateend{\relax}
\newcommand{\abstract}[1]{\def\@abstract{#1}}   % Abstract
\def\@abstract{\relax}
\newcommand{\abstractsecondlanguage}[1]{\def\@abstractsecondlanguage{#1}}   % Abstract
\def\@abstractsecondlanguage{\relax}
\newcommand{\zweitbetreuer}[1]{                 % Name des zweiten Betreuers
   \def\@zweitbetreuer{#1}
   \@zweibetreuertrue
}
\def\@zweitbetreuer{\relax}

% 24.10.2014 Adressblock an unteren Seitenrand hier definiert
\newcommand{\adressblock}
{
	\parbox{14cm}{
	\scriptsize
%	\sffamily											% einkommentieren, falls serifenlose Schrift gewünscht wird
	\begin{tabular}{ll@{\hspace{1.5cm}}ll}
	Postanschrift: & Institut f\"ur Robuste Leistungshalbleitersysteme &
	Tel.: & +49\,(0)\,711\,685\,68700 \\[-0.6ex]
	& Pfaffenwaldring 47 &
	Fax.: & +49\,(0)\,711\,685\,58700 \\[-0.6ex]
	& &E-Mail: & sekretariat@ilh.uni-stuttgart.de\\[-0.6ex]
	& D-70569 Stuttgart &Web: & www.ilh.uni-stuttgart.de  \\[-0.6ex]

	\end{tabular} 
	}
}

%24.10.2014 Kopfzeile mit Uni-Logo und Institutsnamen mit Sprachswitch english <-> deutsch
\newcommand{\logoheader}
{

	\enlargethispage{2cm}
	\vspace*{-1.5cm}
	\ifg@rman
		\raisebox{-0.5cm}{\includegraphics[width=8cm]{vorlage/figures/unilogo_de.pdf}}
	\else
		\raisebox{-0.5cm}{\includegraphics[width=8cm]{vorlage/figures/unilogo_en.pdf}}
	\fi	
	\hspace{0.8em}
%	\rule[-4.55mm]{1pt}{16mm}
	\hspace{0.8em}
	\ifg@rman
		\raisebox{-0.5cm}{\includegraphics[width=8cm]{vorlage/figures/ilh_logo20_full.pdf}}
	\else
		\raisebox{-0.5cm}{\includegraphics[width=8cm]{vorlage/figures/ilh_logo20_full_en.pdf}}
	\fi	
}

% Sprache/Arbeitstyp abhängige Deklarationen
\ifg@rman
   
   \if@diplom
      \newcommand{\rprtn@me}{Diplomarbeit}
   \fi
   \if@studien
      \newcommand{\rprtn@me}{Studienarbeit}
   \fi
   \if@master
      \newcommand{\rprtn@me}{Masterarbeit}
   \fi
   \if@bachelor
      \newcommand{\rprtn@me}{Bachelorarbeit}
   \fi
   \if@forschung
      \newcommand{\rprtn@me}{Forschungsarbeit}
   \fi

   \newcommand{\betreuern@me}{Betreuer}
   \newcommand{\zeitraumn@me}{Zeitraum}
   \newcommand{\denn@me}{den }

   \newcommand{\eiderkln@me}{Eidesstattliche Erklärung}
   \newcommand{\abstractn@me}{Zusammenfassung}
   \newcommand{\abstractsecondlanguagen@me}{Executive Abstract}

\else

   \if@diplom
      \newcommand{\rprtn@me}{Diploma Thesis}
   \fi
   \if@studien
      \newcommand{\rprtn@me}{Master Lab Course}
   \fi
   \if@master
      \newcommand{\rprtn@me}{Master's Thesis}
   \fi
   \if@bachelor
      \newcommand{\rprtn@me}{Bachelor's Thesis}
   \fi
      \if@forschung
      \newcommand{\rprtn@me}{Student Research Project}
   \fi

   \newcommand{\betreuern@me}{Supervisor}
   \newcommand{\zeitraumn@me}{Period}
   \newcommand{\denn@me}{}

   \newcommand{\eiderkln@me}{Statutory Declaration}
   \newcommand{\abstractn@me}{Executive Abstract}
   \newcommand{\abstractsecondlanguagen@me}{Zusammenfassung}

   \newcommand{\glqq}{'}
   \newcommand{\grqq}{'}
\fi

\pagestyle{scrheadings}
\ohead{\pagemark}
\ihead{\headmark}
\ofoot{}

\renewcommand{\baselinestretch}{1.1}
\setlength{\parindent}{0em}
\setlength{\parskip}{0.3em}

\sloppy
\raggedbottom

\newcommand{\eqnbigskip}{
   \setlength{\abovedisplayskip}{1.5em plus0.5em minus0.2em}
   \setlength{\belowdisplayskip}{1.5em plus0.5em minus0.2em}
   \setlength{\abovedisplayshortskip}{1em plus0.7em minus0.1em}
   \setlength{\belowdisplayshortskip}{1em plus0.7em minus0.1em}
}



%Select Thesis language
\ifg@rman
	\usepackage[english, ngerman]{babel}% Set default language to german
\else
	\usepackage[ngerman, english]{babel}% Set default language to english	
\fi

\newcommand{\makeformalities}{
   \ifg@rman
      Vorgelegt von\\
      \textbf{\@author}\\
      %aus \@placeofbirth \\
      am Institut für Robuste Leistungshalbleitersysteme \\
      der Universität Stuttgart\par
   \else
      Submitted by\\
      \textbf{\@author}\\
      %from \@placeofbirth \\
      at the Institute of Robust Power Semiconducter Systems \\
      of the University of Stuttgart\par
   \fi
}



% Titelseite
\renewcommand*{\maketitle}{%
\begin{titlepage}
\logoheader

\begin{center}
%   \sffamily				% einkommentieren, falls serifenlose Schrift gewünscht wird
   \vspace{3cm}
   \vfill
   %\Large{\rprtn@me}
   {
      \linespread{1.1}
	%   \sffamily				% einkommentieren, falls serifenlose Schrift gewünscht wird      
	\centering\LARGE\textbf{\@title}
   }
   \par
   \vspace{1.5cm}
   \Large{\textsc{\rprtn@me}}\\
   \vspace{1.5cm}
   \makeformalities
   \vspace{1.5cm}
   \vfill
   \normalsize
   \begin{tabular}{rl}
      \betreuern@me: & \@betreuer \\
      \if@zweibetreuer
         & \strut \@zweitbetreuer \\[2em]
      \else
         \strut \\[2em]
      \fi
      \zeitraumn@me: & \@datestart\; -- \;\@dateend
   \end{tabular}
   \par
   \vspace{3em}
    Stuttgart, \denn@me\@dateend
\end{center}
\vspace{6em}
\adressblock
\end{titlepage}

\thispagestyle{empty}
\cleardoublepage

{
%% Executive Overview

\pagenumbering{roman}

\ifg@rman
   \addchap{Überblick}
   \begin{tabularx}{\textwidth}{lX}
      Arbeitstyp: & \rprtn@me\\
      Deutscher Titel: & \@title\\
      Englischer Titel: & \@foreigntitle\\
      Student: & \@author\\
      \betreuern@me: & \@betreuer \\
      Abgabe: & \@dateend
   \end{tabularx}
\else
   \addchap{Overview}
   \begin{tabularx}{\textwidth}{lX}
      Type of thesis: & \rprtn@me\\
      English title: & \@title\\
   %   German title: & \@foreigntitle\\
      Student: & \@author\\
      \betreuern@me: & \@betreuer \\
      Date of Examination: & \@dateend
   \end{tabularx}
\fi
\cleardoublepage

%% Eidesstaatliche Erklärung

\addchap{\eiderkln@me}
\vspace*{3.0em}
\ifg@rman
	\noindent Hiermit versichere ich, dass ich die vorliegende Arbeit selbständig und ohne unerlaubte Hilfsmittel unter Beachtung der Regeln zur Sicherung guter wissenschaftlicher Praxis an der Universität Stuttgart in ihrer aktuellen Form angefertigt habe. Ich habe keine anderen als die angegebenen Quellen und Hilfsmitteln benutzt und wörtlich oder inhaltlich übernommene Stellen als solche kenntlich gemacht.
\else
	\noindent We hereby declare that this report is our own work and effort and follows the regulations related to good scientific practice of the University of Stuttgart in its latest form. All sources cited or quoted are indicated and acknowledged by means of a comprehensive list of references.
\fi

\vspace*{2.0em}
\noindent Stuttgart, \denn@me\@dateend

\vspace*{5.0em}
\noindent \@author
%\thispagestyle{empty}
\cleardoublepage

%% Zusammenfassung oder Abstract
\addchap{\abstractn@me}
\vspace*{3.0em}
\@abstract
%\newpage
%\addchap{\abstractsecondlanguagen@me}
%\vspace*{3.0em}
%%Set language for second language abstract
%\ifg@rman
%\selectlanguage{english}
%\else
%\selectlanguage{ngerman}
%\fi
%
%\@abstractsecondlanguage
%
%%Set language back to default
%\ifg@rman
%\selectlanguage{ngerman}
%\else
%\selectlanguage{english}
%\fi


%\thispagestyle{empty}
\cleardoublepage
}
	\newcommand{\makeilhabstract}
	{
		\cleardoublepage
		\pagestyle{empty}
		\logoheader
		\vspace{1cm}
		
		
		\textbf{\@author}\\
		\textbf{\rprtn@me: \@title}\\
		\textbf{\zeitraumn@me: \@datestart\; -- \;\@dateend}\\
		\textbf{\betreuern@me: \@betreuer}
		\if@zweibetreuer
			\textbf{, \@zweitbetreuer} \\
		\else
			\\
		\fi
		\vspace{1cm}
		
		\textbf{\abstractn@me:} \@abstract
		
		\vfill
		\adressblock
	}
	\pagestyle{scrheadings}
}
