# Master Lab Course "Dynamic and Application-Oriented Measurement Techniques for Fast-Switching Power Transistors"

## Abstract

This lab aims to investigate the dynamic characteristics of fast-switching new-age transistors (WBG transistors) and the various techniques involved to measure the electrical parameters while they are operating dynamically. 
Dynamics characteristics help in deducing switching energies and switching losses associated with the transistor and circuit involved. 
These artifacts are found practically using the double pulse test in experiment 3. 
But there are some difficulties and inaccuracies in the double pulse testing method, which can occur at very fast switching transients. 
So, an alternate method called the calorimetric loss measurement method is investigated in experiment 2. 
The core of these experiments lies in the precise measurement of different currents and voltages associated with the setup. 
Thus, the accuracy and bandwidth of various measurement techniques are compared and analyzed in experiments 1 and 4.  