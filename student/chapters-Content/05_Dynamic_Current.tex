%============================================================================================
% CHAPTER
%============================================================================================
\chapter{Dynamic Current Measurements}


%============================================================================================
% SECTION
%============================================================================================
\section{Aim of the experiment}


This experiment is focused on dynamic current measurement in power electronic applications. The dynamic properties of four different current sensors are compared in this experiment: a coaxial shunt resistor, a Pearson current monitor, a Rogowski coil, and a voltage probe for indirect current measurement.


%============================================================================================
% SECTION
%============================================================================================
\section{Preparatory questions}


\begin{enumerate}
    \item Explain the principle of operation of a Pearson transducer

          \begin{enumerate}
              \item[] The Pearson transducer is generally used for measuring high-frequency AC currents in electrical systems. It consists of a toroidal magnetic core, a secondary winding (one-winding device), a resistive termination, and an electromagnetic shield. The working principle is similar to that of a current transformer, where the primary circuit is the path of current flow (conductor under test) along the axis of the toroid, and the voltage induced in the secondary winding is proportional to the current that needs to be measured. But these Pearson monitors differ from conventional current transformers because of their internal termination. This system of distributed terminations extends the usable high-frequency limit well beyond that of a simple transformer. This factory-calibrated termination also permits easy connection to standard voltage-measuring test equipment. But the limitation of this monitor lies in its magnetic core, which causes a limitation on the product of current and time (charge), as the maximum flux is limited by core saturation. It can reliably measure current signals that are within the pass band, whose low and high limits are decided by the secondary winding inductance, terminating resistance, and stray capacitances~\cite{chriswatersPowerConversionIntelligent1986}.
          \end{enumerate}



    \item Explain the principle of operation of a Rogowski coil

          \begin{enumerate}
              \item[] A Rogowski coil is usually a clip around sensor placed around the conductor under test whose current need to be measured. When an AC current flows through a conductor, it induces a voltage in the Rogowski coil which is proportional to the rate of change of current (di/dt) in the conductor. To obtain an output voltage proportional to current, it is necessary to integrate the coil voltage and hence an electronic integrator is used to provide a bandwidth extending down to below 1Hz. A low pass filter is present in parallel with the integrating capacitor to prevent unacceptable DC drift at low frequency (theoretical infinite gain). This filter set the low frequency bandwidth of the current sensor. The distributed inductance and capacitance of the Rogowski coil and length of cable connecting integrator decides the high frequency bandwidth~\cite{HowItWorks}.
                    Advantages over other current measurement techniques:
                    \begin{itemize}
                        \item Non-invasive  and can be installed without interrupting the circuit unlike Pearson.
                        \item Galvanic isolation
                        \item Excellent linearity (saturation at very high di/dt due to air core)
                    \end{itemize}
          \end{enumerate}

    \item Explain the principle of operation of a Shunt resistor

          \begin{enumerate}
              \item[] Shunt resistors work by creating a voltage drop across a “low resistance precision resistor” when current flows through it. So, it works on the principle of Ohm's Law (Required current I = Voltage drop V/ Shunt resistance R). The voltage drop across the shunt is usually in the range of 0 to 100mV. This is typically too small to read by most processors, so the signal is usually amplified and converted to a digital signal. So, the resolution of ADC and precise value of shunt determine the accuracy of the current measured. Currently, there are shunts with a built-in amplifier called the Smart Shunt, and even with built-in ADC, and output to CAN, MODBUS, or RS485~\cite{hovsepianWhatAreShunt2022}.

          \end{enumerate}

    \item What should be considered when measuring current with a shunt?

          \begin{enumerate}
              \item[] A shunt is an invasive (affect the actual output of the original circuit) and intrusive (circuit need to be opened for installation) type current sensor. So, there are some added parasitic effects and conduction losses which need to be considered. Hence, it is important to choose a shunt resistor which has a low resistance to minimize the voltage drop and power dissipation, but also have a high enough resistance to have a measurable voltage drop across it.
          \end{enumerate}

    \item What is the principle of operation of current measurement with a current clamp?

          \begin{enumerate}
              \item[] The current clamp allows for non-intrusive, real-time current
                    measurement without needing to physically disconnect the circuit. It can be used for both AC and DC current measurements. There are two main types (principle involved) in current clamps: magnetic induction clamps- core and coreless (AC measurement) and Hall effect clamps
                    (AC and DC measurement).

                    Magnetic induction clamps - The magnetic field produced by the conductor induces a voltage proportional to the rate of change of the current.

                    Hall effect clamps - An array of sensors (usually Hall sensors) in the current clamp samples the magnetic field generated by the current passing through the conductor at various points, calculates the total field, and outputs a signal proportional to the current.
          \end{enumerate}

    \item What other types of current measurement do you know?
          \begin{itemize}
              \item Hall effect sensors
              \item  Magnetoresistive sensors
              \item Fiber-optic current sensors
              \item Flux gate sensors
              \item GMR (Giant Magnetoresistance) Sensor
          \end{itemize}

    \item What is the difference between a "current-correct" measurement and a "voltage-correct" measurement?

          \begin{enumerate}
              \item[] If ideal measuring instruments were available, i.e., an ammeter with a negligible internal resistance and a voltmeter with an infinitely high internal resistance, both circuits would yield the same result. However, an ammeter has minimal internal resistance, and a voltmeter has less than infinity internal resistance. The voltage-correct circuit connects the voltmeter in parallel to only the component and not to the ammeter, so that the small voltage drop due to the ammeter's internal resistance will not reflect in the voltmeter. Similarly, in the current-correct measurement circuit, the voltmeter is connected in parallel to both the component and the ammeter, so that there is no measured current flowing through the voltmeter branch~\cite{MeasurementOhmicResistances} (\Cref{fig:current_and_voltage}).
          \end{enumerate}


          \begin{figure}
              \centering
              \includegraphics[width=1\linewidth]{figures/current and voltage correct.png}
              \caption{Voltage-correct circuit vs Current-correct circuit~\cite{MeasurementOhmicResistances}}
              \label{fig:current_and_voltage}
          \end{figure}

    \item Explain the difference between intrusive and non-intrusive current measurement and assign the measurement methods you know to these two categories.

          \begin{enumerate}
              \item[] Intrusive and non-intrusive current measurement techniques are differentiated based on whether the measurement method requires direct electrical contact with the circuit being measured or not.
                    \begin{itemize}
                        \item Intrusive: Hall effect sensor, shunt resistor, and current transformer
                        \item Non-intrusive: current clamp, Rogowski coil
                    \end{itemize}
          \end{enumerate}

    \item How fast would the current measurement have to be in order to detect a short circuit in a half-bridge in time and to transfer the circuit to a safe state in order to protect the components from further damage?

          \begin{enumerate}
              \item[] Fault handling time interval (FHTI) = fault detection time + fault response time. This handling time should be less than the Fault tolerance time interval (FTTI), which is a few us to ns in this case. The speed at which current measurements need to be taken and processed depends on several factors, including the characteristics of the power transistors used, the switching frequency of the half-bridge, and the nature of the load. The detection circuit not only has a current sensor but also an ADC, whose response time needs to be accounted for here.
          \end{enumerate}

    \item Consider what influence higher voltages and currents could have on the current measurement.
          \begin{enumerate}
              \item[] \textbf{Influence of Higher Voltages:}
                    Higher voltages have the potential to cause notable common-mode noise. Devices with a high Common Mode Rejection Ratio (CMRR) are necessary to precisely measure the current without being affected by this noise. High voltages can also affect the accuracy of measurement devices due to voltage division effects. Proper attenuation or isolation techniques are necessary to mitigate these effects.

              \item[] \textbf{Influence of Higher Currents:}
                    Higher currents causes increased conduction losses, which can significantly heat up the measurement device (e.g., shunt resistors). This heating can alter the resistance
                    value, affecting accuracy.  Strong magnetic fields produced by high currents have the potential to interfere with adjacent measuring circuits. Proper shielding and separation of measurement paths from high-current conductors are necessary. Moreover, instruments must also have an appropriate measurement range to handle high currents without saturation or damage.
          \end{enumerate}

\end{enumerate}


%============================================================================================
% SECTION
%============================================================================================
\section{Circuit Diagram and Probing}


\Cref{fig:E4_setup} shows a simplified circuit diagram of the experiment. Due to the design of the circuit board, the coaxial shunt resistor and the Rogowski coil measure the current from the source terminal while the Pearson converter measures the current of the commutation cell, therefore a small offset exists between these measurements. To protect the oscilloscope in the event of a coaxial shunt fault, a $50 \Omega$ termination resistor is used at the channel 1 input.

\begin{figure}
    \centering
    \includegraphics[width=.8\textwidth]{figures/E4_setup.png}
    \caption{Measurement setup}
    \label{fig:E4_setup}
\end{figure}


%============================================================================================
% SECTION
%============================================================================================
\section{Measurement Procedure}


%============================================================================================
% SUBSECTION
%============================================================================================
\subsection{Low voltage test without Deskew}


\subsubsection{Experiment Preparation}


The purpose of this section is twofold: firstly, to familiarize the participants with the experimental set-up and secondly to measure the "droop" of the current sensors.
A long enough current pulse is generated, so that an exponential curve (due to the charging of the RL circuit) is recognizable.

\begin{itemize}
    \item Check wire connections (see Illustration experiment setup \Cref{fig:E4_setup}).
    \item Set DC source - channel 1 for "Driver supply" and switch it on.
    \item Check/execute function generator setting and switch it on.
    \item Set the DC source - channel 2 for "DC supply" but do not switch on yet.
\end{itemize}


\subsubsection{Measurement execution}


\begin{enumerate}
    \item Compensate the HV and LV probes.
    \item Set HV probe to 1:10.
    \item Set up the test bench:

          \begin{itemize}
              \item Connect all measurement sensors to the oscilloscope.
              \item Position the Rogowski coil so that it surrounds the source terminal of the transistor.
          \end{itemize}
    \item Settings for the function generator:
          \begin{itemize}
              \item Amplitude: 5 Vpp
              \item Offset: 2.5 V
              \item Pulse width: 3.5 ms
              \item Duty cycle: 50 percent
          \end{itemize}
    \item Setting of the oscilloscope according to \cref{tab:E4_probe_setting}:

          \begin{table}[!ht]
              \centering
              \begin{tabular}{ccccc}
                  \toprule
                  \textbf{Channel} & \textbf{Sensor} & \textbf{Impedance} & \textbf{Scaling} & \textbf{Offset} \\  \midrule
                  1                & Coaxial Shunt   & 1~M$\Omega$        & 0.0501           & 0               \\
                  2                & Pearson Monitor & 50~M$\Omega$       & 0.5              & 0               \\
                  3                & Rogowksi coil   & 1~M$\Omega$        & 20m              & 200m/100m       \\
                  4                & Voltage probe   & 1~M$\Omega$        & 10:1             & 0               \\
                  \bottomrule
              \end{tabular}
              \caption{Setting of different probes}
              \label{tab:E4_probe_setting}
          \end{table}


    \item Configuration of the voltage source:
          \begin{itemize}
              \item Channel 1: Set 12V for the gate driver and switch it on by pressing the "On" button. The current consumption of the gate driver should be between 43 mA and 47 mA.
              \item Channel 2: Set 2V as UDC and switch on by pressing the "On" button.
          \end{itemize}
    \item Triggering the oscilloscope: Click on "Single" in the upper right corner. The oscilloscope is set to "Average 5", so five pulses are required before the oscilloscope shows an image.
    \item Click the "Play" icon on the function generator surface five times to generate five pulses. An exponential increase in the current should be visible.

\end{enumerate}


%============================================================================================
% SUBSECTION
%============================================================================================
\subsection{After Deskewing}


\subsubsection{Measurement Execution}


De-Skew of the current sensors:
\begin{enumerate}
    \item Set the impedance of channels 1 to 3 to the values:
          \begin{itemize}
              \item Channel 1: 1~M$\Omega$
              \item Channel 2: 50~$\Omega$
              \item Channel 3: 50~$\Omega$
          \end{itemize}
    \item Assuming a delay of 50.78 ps/cm from an RG58 coaxial cable, calculate and set the "skew" of channels 2 and 3. For the Rogowski coil use the specification from the datasheet.
    \item Start the measurement with a new pulse.
    \item Compare the results of the measurement without and with the De-Skewing
\end{enumerate}

The used coaxial cable had a length of 1~m, therefore the calculated delay is 5.078~ns.
The Rogowski coil has a specified delay of 40-50~ns.
Since the same length of coaxial cable was used for the Pearson current monitor, the delay is 5.078~ns.


%============================================================================================
% SECTION
%============================================================================================
\section{Results}


%============================================================================================
% SUBSECTION
%============================================================================================
\subsection{Measurement Evaluation for Part 1.}


\Cref{fig:E4_1} shows the measurement currents by different current sensors before deskew.

a) Pearson Monitor:

The current rises up at the start but begins to drop because the core of Pearson transformer which reaches saturation at 0.4~ms. By placing the pointer at that point, we found the charge to be 585~$\mu$C.(\cref{fig:E4_1pearson}) This saturation of Pearson transducer limits its ability to measurement higher currents.
It has a low dynamic range and works better at high frequency applications.

b) Rogowski Coil :

The measured current rises slowly at the start and droops a little at the end.
The measured droop is 0.13232A from the peak to the final value.
A Rogowski coil can't accurately measure low frequency currents due to phase and amplitude errors.
Those errors occur when dealing with continuous current while a droop in the measurement occurs in case of transient currents.
The amplitude of droop depends on the time constant of the integrator used in the Rogowski coil.
The droop offset can be seen when the current returns to zero but the Rogowski measurement undershoots and measures -2A(\Cref{fig:E4_1rogowski})
\cite{DealingDroopRocoil2019}.

c) Since the current values of the coaxial shunt before de-skewing (\Cref{fig:E4_1}) seem to be wrong, the data from the second measurement are taken here (\Cref{fig:E4_2}).
Before turn-off, $V_{DS} = 283mV$ and $I_{coax} = 7.78A$ which results in an on-resistance of $R_{DS,on} = 36~m\Omega$.
The stated on-resistance of the GA04JT17-247 SiC transistor is $180~m\Omega$ at 25~°C which is almost five times as large.
In reality, the measured resistance should be bigger that the actual resistance because we can only measure the resistance of the whole assembly.

d) Calculate the time constant of the assembly:
    \begin{align}
        \frac{di}{dt} & = \frac{7.78A}{0.0035s} = 2222 \frac{A}{s}                                  \\
        L             & = \frac{U_{DC} }{\frac{di}{dt}} = \frac{283mV}{2222\frac{A}{s}} = 127~\mu H \\
        \tau          & = \frac{L}{R} = \frac{127~\mu H}{36m~\Omega} = 3.5ms
    \end{align}
    \label{eqn:E4_timeC}


e) The rise in voltage occurs due to the fact that switched node voltage rises to maximum value after turn off, which is followed to current commutation. The voltage overshoot occurs due to parasitic inductance and ringing can be due to parasitic capacitance.




\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/Pearson_charge.eps}
    \caption{Response of Pearson transducer}
    \label{fig:E4_1pearson}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/E4_1_Rogowski_droop.eps}
    \caption{Rogowski droop}
    \label{fig:E4_1rogowski}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/E4_1_comparison_wrong.eps}
    \caption{comparing current sensors without de-skew, current of coax shunt seems too low}
    \label{fig:E4_1}
\end{figure}


%============================================================================================
% SUBSECTION
%============================================================================================
\subsection {Measurement Evaluation for Part 2}

With an assumed 50.78~ps/cm delay, the RG58 coaxial cable used has a delay of 5.078ns at 1~m length.
This value is used for the coaxial shunt and Pearson current sensor.
For the Rogowski coil, 40-50ns was specified in the datasheet.

\Cref{fig:E4_1} shows the measurement currents by different current sensors before deskew.
The current and voltage waveforms look better after deskew (\Cref{fig:E4_2}). 
For the Rogowski coil we also changed the input impedance at the oscilloscope from 1~M$\Omega$ to 50~$\Omega$
This reduced the sensitivity of the Rogowski coil and the maximum value is reduced from 8~A to 6.2~A.
The droop in Rogowski coil is more pronounced after deskew.
The curve of the coaxial shunt had a considerably lower maximum value before de-skew which is  probably from a setup error.
Additionally, the coax curve had a faster risetime and settled earlier before de-skew.

The maximum de-skew value was 40-50~ns for the Rogowski coil which is at least one order of magnitude smaller than our measurements which are in the $\mu$s to ms range.
Therefore, the de-skew is not visually noticeable but for power calculations it would lead to more accurate results.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{figures/E4_2_comparison.eps}
    \caption{Comparing current sensors after de-skew}
    \label{fig:E4_2}
\end{figure}

