%============================================================================================
% CHAPTER
%============================================================================================
\chapter{Measurement of the switching energies of a SiC power MOSFET in a half bridge configuration}


%============================================================================================
% SECTION
%============================================================================================
\section{Aim of the experiment}


In this experiment, a method for extracting the turn-on and turn-off energy of a GaN power
transistor is demonstrated. For this purpose, a double pulse test (DPT) is performed.
Furthermore, the difficulties and challenges of performing such a test with GaN power
transistors will be highlighted.
This experiment differs from the theoretical DPT setups in two aspects:

\begin{itemize}
    \item Instead of a DPT fixture tailored to characterize the device under test (DUT), a half bridge power cell is used. The board includes parasitic elements that affect the switching behaviour of the DUT.
    \item Instead of two pulses, a burst of pulses is used to switch ON and OFF at different current values in a single oscilloscope trace.
\end{itemize}
All measurements are performed at $V_{DC} = 50V.$ A $ 27 \mu H$ air core coil is used for the test.
The low side transistor is driven with a total of four pulses. The high side transistor is
switched off all the time. It only carries current in the reverse direction in freewheeling state.
Afterwards, the measured signals are evaluated on the oscilloscope in order to determine the
switching energy. By using the integral function and markers, the corresponding switching
energies for the individual pulses can be determined. According to the definition, the
switching energy can be calculated as follows:

\begin{align}
    E_{on} = \int_{t_{ids,10\%}}^{t_{vds,10\%}} V_{ds} \cdot I_{ds} \cdot \,dt \label{eqn:E_ON} \\
    E_{off} = \int_{t_{vds,10\%}}^{t_{ids,10\%}} V_{ds} \cdot I_{ds} \cdot \,dt \label{eqn:E_OFF}
\end{align}

The double pulse test can be performed with different DUT-boards. The procedures for execution and evaluation do not differ.
The different boards demonstrate different challenges and difficulties, which can lead to errors and problems in such a DPT.

DUT-Boards:
\begin{itemize}
    \item DUT-1: Reduced switching speed, $R_{G,N} = R_{G,P} = 10 \Omega$.
    \item DUT-2: Increased switching speed, $R_{G,N} = R_{G,P} = 30 \Omega$.
\end{itemize}


%============================================================================================
% SECTION
%============================================================================================
\section{Preparatory questions}


\begin{enumerate}


    \item Describe the terms "hard switching" and "soft switching." Are there any differences between these two modes of operation in terms of the switching energies of the power transistors involved?

          \begin{enumerate}
              \item[] Hard switching occurs when a power transistor switches ON or OFF under conditions where high voltage and current overlap. Soft Switching techniques aim to minimize the overlap of high voltage and high current during switching events. There are two primary techniques of soft switching: Zero Voltage Switching (ZVS) and Zero Current Switching (ZCS). In the ZVS during turn OFF, voltage rise is delayed by snubber circuits so that current falls early. In ZCS during turn ON, current rise is delayed by inductive elements so that voltage falls early. This simultaneous presence of high voltage and high current during hard switching results in high switching losses as well as high stress on the transistor and high EMI generation. The switching energies in soft switching are significantly less (in ideal cases, zero) as there is minimal to no overlap of voltage and current.

          \end{enumerate}
    \item Explain the operation of a double pulse test. What are the advantages and disadvantages of this measurement method?

          \begin{enumerate}
              \item[] Circuit Setup:

                    The test circuit typically includes a DC power supply, a gate driver, the DUT (e.g., a MOSFET), a load inductor, a freewheeling diode, and measurement equipment such as an oscilloscope with voltage and current probes.

                    \textbf{Phase 1: On State}

                    The gate driver applies a first pulse to turn on the DUT. When the DUT turns on, the current through the inductor starts to ramp up linearly. The voltage across the DUT and the current through the inductor are measured during this period. This gives ON state conduction loss.

                    \textbf{Phase 2: Turn Off}

                    After the first pulse, the gate driver turns off the DUT. The switch node voltage rises to its maximum value, followed by current commutation into the freewheeling diode, eventually leading to zero current (apart from leakage current). Here, turn-off switching energies can be calculated.

                    \textbf{Phase 3: Off-State}

                    The current through DUT has reached close to zero, and the load current has decreased as well. Here, the off-state conduction loss of DUT and the on-state conduction loss of the freewheeling diode can be calculated.

                    \textbf{Phase 4: Turn on}

                    The current path again switches to DUT. This leads to a rise in current through DUT and a drop in switch node voltage. Here, turn-on switching energies can be calculated.

                    \begin{figure}
                        \centering
                        \includegraphics[width=.8\textwidth]{figures/DPT.png}
                        \caption{Waveforms of DPT~\cite{anyaWBGProductsValidation}}
                        \label{fig:E3_DPT}
                    \end{figure}


                    \textbf{Advantages of the Double Pulse Test:}

                    \begin{itemize}

                        \item Detailed switching characteristics and a standard method for analysing dynamic characteristics across industries
                        \item Identification of Parasitic Effects
                        \item Benchmarking and comparison with the manufacturer's artefacts

                    \end{itemize}


                    \textbf{Disadvantages of the Double Pulse Test:}
                    \begin{itemize}

                        \item Inaccurate during fast switching transients and soft switching scenarios
                        \item The test setup can be complex, requiring precise control and synchronization of the gate pulses, as well as accurate measurement equipment.
                        \item Less repeatability, i.e., output depends on measurement setup.
                        \item Limited real-world replication
                    \end{itemize}
          \end{enumerate}

    \item On page 3, explain the procedure for determining the switching energies. What happens if the voltage is reduced by a factor of 10?
          \begin{enumerate}
              \item[] Switching energies are calculated according to the \cref{eqn:E_ON,eqn:E_OFF}
                    As per these equations, Eon and Eoff is reduced by a factor of 10 if voltage is reduced by a factor of 10 assuming all other parameters stay the same.
          \end{enumerate}


    \item Although the switching operations are considered at the same current, the switch-off energy Eoff is different from the switch-on energy Eon, what could be the reason for this?
          \begin{itemize}
              \item Parasitic capacitance and inductance
              \item Diode reverse recovery
              \item Different Ron and Roff
              \item Non linearity of switching on and off graph
          \end{itemize}



    \item What alternative measurement methods are available for determining the switching energy of power transistors?
          \begin{itemize}
              \item  Calorimetric switching loss Method
              \item  Multiple pulse method (n pulse instead of DPT)
              \item  Continuous Operation Method (not suitable for high power applications)
          \end{itemize}



    \item  What is the relevance of the knowledge of the switching losses of power transistors?
           
          \begin{itemize}
                \item[] The knowledge of switching losses of power transistors can significantly boost the performance of the device. It can help in
              \item  Improving Efficiency of the system
              \item  Building better heat dissipation system
              \item  Better design size Optimization
              \item  Component Selection,  Optimization gate drive circuits, snubber circuits, etc.
              \item  Reduction of Operational Costs: Lower switching losses directly translate to reduced energy consumption
          \end{itemize}



    \item What difficulties may be encountered in simultaneously measuring current and voltage to determine switching point power dissipation?

          \begin{enumerate}
              \item[] If Deskewing is not done properly, it can lead to a delay between the voltage and current measurements which further leads to incorrect energy loss calculations. This will ultimately lead to wrong thermal designs which can cause huge safety issues.
          \end{enumerate}


    \item What is a coaxial shunt, and how does it work?

          \begin{enumerate}
              \item[] 	The coaxial current shunts are used for measurement of the DC and AC currents over a wide frequency band [up to 2 GHz]. It consists of current and voltage pins and structurally consists of two cylinders embedded as one, on which the current flows in opposite directions (\cref{fig:E4_shunt_construction}). The inner cylinder (resistive tube) is manufactured with high resistivity material (e.g., manganin) and the external cylinder with lower resistivity material (e.g., copper). The coaxial construction has the advantage that the current flowing in the shunt produces negligible electromagnetic fields outside the air gap between the cylinders. As a result, the measuring circuit is not inductively coupled with the shunt current, and so the output voltage has a very small inductive component (low insertion inductance)~\cite{johnsonCurrentMeasurementUsing1992}. The field in the air gap propagates into the cylinders on either side at a rate determined by the skin depth and resistivity. The output is obtained by measuring the voltage between a pair of distinct points on the resistive tubes, which is then converted to current as per Ohm's law.
              
              \begin{figure}[ht]
                \centering
                \includegraphics[width=0.4\textwidth]{figures/E4_shunt_construction.png}
                \caption{Construction of a coaxial current shunt}
                \label{fig:E4_shunt_construction}
              \end{figure}

          \end{enumerate}


    \item How must the coaxial shunt be connected to measure the transistor current $I_{ds}$?

          \begin{enumerate}
              \item[] To measure $I_{ds}$, coaxial shunt needs to connected in the drain source path. Generally, it is connected to the source terminal rather than drain for ensuring an easy path to the ground and the current measured will be more or less the same in both cases. This means inner conductor (signal) is connected to source terminal and the outer conductor(ground) to the circuit ground. This way, there is no possibility of current flowing through the oscilloscope ground, distorting the measurement.  Similarly, the inner conductor of the coaxial cable is connected to the positive  terminal of oscilloscope and outer conductor to the negative terminal (ground) of oscilloscope. Also, proper calibration needs to be done to establish the relation between the voltage in coaxial shunt and the actual drain source current.
          \end{enumerate}


    \item You can see oscillations and over voltages in the signal curve of some measured
          variables. What are possible causes of errors, and how can they be reduced?

          \begin{itemize}

            \item \textbf{Parasitic Capacitance:} 
            The primary reason for ringing and oscillations in voltage and current signals is the parasitic capacitance (mainly Switch node capacitances) in the circuit can cause oscillations and ringing in the voltage and current signals. Some ways to reduce this are by careful component selection with low capacitances, optimized PCB layout, minimizing the length of the connection leads and employing low-capacitance cables.

            \item \textbf{Parasitic Inductance:}             
            The primary reason for transient voltage overshoot is the parasitic inductances in the circuit whose amplitude is determined by the rate of change of transistor current and inductances. To minimize the inductive effects, we can employ optimized PCB layout techniques like shortening the loop area of the conductors, avoiding sharp bends and routing traces close to the reference planes, and also by applying proper grounding to the measurement setup, using short cables, etc.
            
            \item \textbf{Switching Transients:} 
             Oscillations and voltage overshoots can also occur due to the rapid switching of power transistors (high switching frequency). So proper gate-driving techniques need to be employed to avoid this. This includes using appropriate gate resistors to dampen ringing, minimizing the gate driver loop area, etc.
          \end{itemize}


\end{enumerate}


%============================================================================================
% SECTION
%============================================================================================
\section{Measurement Procedure}


%============================================================================================
% SUBSECTION
%============================================================================================
\subsection{Double pulse Test}


\begin{figure}[hbt]
    \centering
    \includegraphics[width=.8\textwidth]{figures/E3_setup.png}
    \caption{Measurement setup}
    \label{fig:E3_setup}
\end{figure}


\subsubsection{Experiment Preparation}


Check the prepared Setup (\cref{fig:E3_setup}) and make the following settings:

\begin{enumerate}

    \item Logic Power supply:
          \begin{itemize}
              \item Output Voltage: 24V
              \item Output Current: 500 mA
          \end{itemize}

    \item Power Supply:

          \begin{itemize}
              \item Output Voltage: 50 V.
              \item Output Current: 3 A
          \end{itemize}

    \item Function Generator:

          \begin{itemize}
              \item Pulse, Burst, Trig. Man.
              \item Frequency: 250 kHz.
              \item High Level / Low Level: 3.3 V / 0 V
              \item Pulse Width: 2.70us
              \item Cycle Count: 4
          \end{itemize}

\end{enumerate}


\subsubsection{Measurement execution}


Complete, step by step, the following instructions
\begin{enumerate}
    \item Check that Channel 1+2 are off, (1) displays 0V and 0A.
    \item Place the DUT-1 power cell on the DPT-Board
    \item Connect all the Probes to the DPT-Board.
          \begin{itemize}
              \item Vgs: Passive probe (Clip) between Gate and Source of Q2.
              \item Vds:  HV passive probe (SMA) between Drain and Source of Q2.
              \item Vshunt:  IsoVu (MMCX) between source Q2 and GNDPOWER.
              \item IL: Current probe at the Load
          \end{itemize}
    \item Connect the function generator to the corresponding PWM connector (PWM LS)
    \item Now set the oscilloscope that the following requirements for the measurement are fulfilled
          \begin{itemize}
              \item The timescale should cover all 4 pulses of the measurement of the voltages (Vsw, Vgs and Vshunt) and the current (IL) should be seen completely.
          \end{itemize}
    \item Also include the following maths functions into the oscilloscope
          \begin{itemize}
              \item Maths 1(M1): To calculate the drain to source current of Q2 measured by the current shunt, divide the measured shunt voltage by the value of the shunt.
              \item Maths 2(M2): Is the instant power calculated by Vds*Ids
              \item Maths 3(M3): Is the accumulated energy from the origin of the oscilloscope
                    screen, integrating the instant power in time: \( E = \int_{}^{} P\,dt \) 
          \end{itemize}

    \item Set +24V/0.5 A in the Channel1 of the Voltcraft DLP 3603 power supply and enable the output by pushing the "On" button. The logic supply is now active
    \item Set the Function Generator to the above-mentioned settings. After that, Enable the output by pressing "Channel1", (1). The PWM signal burst is already configured in the Agilent 33500B function generator. The signal generator is ready to be triggered by pushing the "Trigger" button.
    \item Press the trigger button to check if the pulses are visible on the oscilloscope.
    \item  Now Enable Channel 2 of the Voltcraft DLP 3603. The DC Link voltage is now on.
    \item Push now the "trigger" button of the Keysight 33500B/3600A function generator. The oscilloscope should capture the waveforms and show the results on its display.
    \item After the measurement, disable Channel 2, so there is no DC-Link voltage any more.
    \item Repeat with DUT-2


\end{enumerate}


%============================================================================================
% SECTION
%============================================================================================
\section{Results}


The switch ON and switch OFF energies of 2 DUTs are found using DPT and are noted in \cref{tab:tE3_Esw_DUT1} and \cref{tab:E3_Esw_DUT2}. All $E_{on}$ / $E_{off}$ measurements compared in \cref{fig:E3_all_Eon_DUT1,fig:E3_all_Eoff_DUT1,fig:E3_all_Eon_DUT2,fig:E3_all_Eoff_DUT2}.
There are only $E_{off}$ and no $E_{on}$ values for the last current pulses because of the nature of the test.
It logically has to end with a switch-off step, so there is no switch-on value for the last step.
The graphs of Vds, Id, Power loss and switching energies at various currents are plotted in a series of graphs.(\cref{E3_wave_5.5A,E3_wave_10.2A,E3_wave_15A,E3_wave_19.2A,E3_wave_7.3A,E3_wave_12.6A,E3_wave_16.9A,E3_wave_21.2A})

It is readily apparent from the table and graphs that the DUT with lower value of gate resistors have lower switching energies - DUT1, compared to the one with higher value of gate resistors - DUT 2. This is due to the fact that lower gate resistors lead to faster switching and in turn low switching loss.
Another difference is that during turn-off there is a small amount of voltage overshoot of $V_{DS} $ with DUT2.
This is because the gate resistor dampens the oscillations or overshoots caused by parasitic inductances.
DUT2 has a lower gate resistance, so it has overshoots but no significant ringing yet.



\begin{figure}[!ht]
    \centering
        \centering
        \includegraphics[width=.8\textwidth]{figures/E3_Rg10_all_Eon.eps}
        \caption{All $E_{on}$  measurements compared for DUT1 ($R_G = 10~\Omega$) }
        \label{fig:E3_all_Eon_DUT1}
\end{figure}

\begin{figure}[!ht]
        \centering
        \includegraphics[width=.8\textwidth]{figures/E3_Rg10_all_Eoff.eps}
    \caption{All  $E_{off}$ measurements compared for DUT1 ($R_G = 10~\Omega$) }
    \label{fig:E3_all_Eoff_DUT1}
\end{figure}

\begin{figure}[!ht]
    \centering
        \centering
        \includegraphics[width=.8\textwidth]{figures/E3_Rg30_all_Eon.eps}
        \caption{All $E_{on}$  measurements compared for DUT2 ($R_G = 30~\Omega$) }
        \label{fig:E3_all_Eon_DUT2}
\end{figure}

\begin{figure}[!ht]
        \centering
        \includegraphics[width=.8\textwidth]{figures/E3_Rg30_all_Eoff.eps}
    \caption{All  $E_{off}$ measurements compared for DUT2 ($R_G = 30~\Omega$) }
    \label{fig:E3_all_Eoff_DUT2}
\end{figure}



\begin{table}[!ht]
    \centering
        \begin{tabular}{cccc}
        \toprule
        \textbf{Current  [A]} & $\mathbf{E_{on}}$ \textbf{[uJ]} & $\mathbf{E_{off}}$ \textbf{[uJ]} & $\mathbf{E_{total}}$\textbf{[uJ]} \\ \midrule
        5.5                   & 13.857                          & 10.083                           & 23.94                             \\
        10.2                  & 22.976                          & 18.443                           & 41.419                            \\
        15                    & 33.389                          & 29.162                           & 62.551                            \\
        19.2                  & N/A                             & 33.864                           & N/A                               \\
        \bottomrule
    \end{tabular}
    \caption{Comparison of switching energies at different currents for DUT1 ($R_G = 10~\Omega$)}
    \label{tab:tE3_Esw_DUT1}
\end{table}



\begin{table}[!ht]
    \centering
        \begin{tabular}{cccc}
        \toprule
        \textbf{Current  [A]} & $\mathbf{E_{on}}$ \textbf{[uJ]} & $\mathbf{E_{off}}$ \textbf{[uJ]} & $\mathbf{E_{total}}$\textbf{[uJ]} \\ \midrule
        7.3                   & 22.885                          & 19.518                           & 42.403                            \\
        12.6                  & 36.684                          & 31.749                           & 68.433                            \\
        16.9                  & 45.606                          & 40.192                           & 85.798                            \\
        21.2                  & N/A                             & 49.941                           & N/A                               \\
        \bottomrule
    \end{tabular}
    \caption{Comparison of switching energies at different currents for DUT2 ($R_G = 30~\Omega$)}
    \label{tab:E3_Esw_DUT2}
\end{table}

%======================
% DUT1
%======================


\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg10_Eon1.eps}
        \caption{$E_{on}$}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg10_Eoff1.eps}
        \caption{$E_{off}$}
    \end{subcaptionblock}%
    \caption{DUT1 ($R_G = 10~\Omega$), Waveform for 5.5A}
    \label{E3_wave_5.5A}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg10_Eon2.eps}
        \caption{$E_{on}$}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg10_Eoff2.eps}
        \caption{$E_{off}$}
    \end{subcaptionblock}%
    \caption{DUT1 ($R_G = 10~\Omega$), Waveform for 10.2A}
    \label{E3_wave_10.2A}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg10_Eon3.eps}
        \caption{$E_{on}$}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg10_Eoff3.eps}
        \caption{$E_{off}$}
    \end{subcaptionblock}%
    \caption{DUT1 ($R_G = 10~\Omega$), Waveform for 15A}
    \label{E3_wave_15A}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg10_Eoff4.eps}
        \caption{$E_{off}$}
    \end{subcaptionblock}%
    \caption{DUT1 ($R_G = 10~\Omega$), Waveform for 19.2A}
    \label{E3_wave_19.2A}
\end{figure}

%======================
% DUT2
%======================


\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg30_Eon1.eps}
        \caption{$E_{on}$}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg30_Eoff1.eps}
        \caption{$E_{off}$}
    \end{subcaptionblock}%
    \caption{DUT2 ($R_G = 30~\Omega$), Waveform for 7.3A}
    \label{E3_wave_7.3A}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg30_Eon2.eps}
        \caption{$E_{on}$}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg30_Eoff2.eps}
        \caption{$E_{off}$}
    \end{subcaptionblock}%
    \caption{DUT2 ($R_G = 30~\Omega$), Waveform for 12.6A}
    \label{E3_wave_12.6A}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg30_Eon3.eps}
        \caption{$E_{on}$}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg30_Eoff3.eps}
        \caption{$E_{off}$}
    \end{subcaptionblock}%
    \caption{DUT2 ($R_G = 30~\Omega$), Waveform for 16.9A}
    \label{E3_wave_16.9A}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E3_Rg30_Eoff4.eps}
        \caption{$E_{off}$}
    \end{subcaptionblock}%
    \caption{DUT2 ($R_G = 30~\Omega$), Waveform for 21.2A}
    \label{E3_wave_21.2A}
\end{figure}
