%============================================================================================
% CHAPTER
%============================================================================================
\chapter{Calorimetric Measurement of Switching Losses}


%============================================================================================
% SECTION
%============================================================================================
\section{Aim of the experiment}


The goal of the experiment is the measurement of the switching energy of the LMG3410 GaN Power Stage using transient calorimetric measurement. The benefit of transient $Z_{th}$ calibrated measurements is emphasized. Observation of dead-time-dependent change of switching losses is carried out. The necessity of calorimetric characterization for transistors involving soft switching is recognized.


%============================================================================================
% SECTION
%============================================================================================
\section{Preparatory questions}


\begin{enumerate}
    \item With which other techniques could you measure the switching energies, and what are the advantages / disadvantages of each technique?
          \begin{itemize}
              \item[] Double pulse test can be used to calculate the switching energies.
              \item[] \textbf{Advantages of the Double Pulse Test:}

              \item Detailed switching characteristics and a standard method for analysing dynamic characteristics across industries
              \item Identification of Parasitic Effects
              \item Benchmarking and comparison with the manufacturer's artefacts

          \end{itemize}

          \begin{itemize}
              \item[] \textbf{Disadvantages of the Double Pulse Test:}

              \item Inaccurate during fast switching transients and soft switching scenarios
              \item The test setup can be complex, requiring precise control and synchronization of the gate pulses, as well as accurate measurement equipment.
              \item Less repeatability, i.e., output depends on measurement setup.
              \item Limited real-world replication
          \end{itemize}

    \item We are using NTC temperature sensors in this experiment. What does NTC and respectively PTC mean in this context? Name a few common temperature sensors and allocate them to NTC or PTC behaviour.
          \begin{itemize}
              \item \textbf{NTC:} Negative temperature coefficient (NTC) sensors are those whose resistance drops with increase in temperature. \\ Example: Vishay NTC Thermistor series, Silicon diode temperature sensors like LM94022 from TI, some NTC RTDs.
              \item \textbf{PTC:} Positive temperature coefficient (PTC) sensors are those whose resistance increases with increase temperature \\ Example: Silicon based PTC like STTS22H, IC based like MAX30205, EPCOS PTC thermistors, PT100 RTD sensor.
          \end{itemize}
\end{enumerate}


%============================================================================================
% SECTION
%============================================================================================
\section{Measurement Procedure}


%============================================================================================
% SUBSECTION
%============================================================================================
\subsection{Calibration Measurement}


In the calorimetric measurement, the occurring power dissipation is determined from the temperature change of the semiconductor or near-semiconductor sensors $T_{TS}(t)$. Since the relationship between temperature and power loss depends strongly on the setup, the measurement setup must be calibrated by determining the thermal impedance $Z_{th,TS-amb}$. For this purpose, electrically easy-to-measure conduction losses ($P_{cal} = I_{cal} \cdot V_{cal}$) are applied to the half-bridge and the temperature step response is measured.


\subsubsection{Experiment Preparation}


Check the prepared Setup (\cref{fig:E2_setup}) and make the following settings:

\begin{figure}
    \centering
    \includegraphics[width=.8\textwidth]{figures/E2_setup.png}
    \caption{Measurement setup}
    \label{fig:E2_setup}
\end{figure}

\begin{enumerate}
    \item Voltage supply:
          \begin{itemize}
              \item Base Board Supply connected with CH1 (12V, 300mA)
              \item DC Link connected with CH2 (Ical, 2.5V)
              \item Fan connected with CH3 (24V, 200 mA)
              \item Temperature sensor supply with battery-pack 5V. Polarity is important! Check DUT
              \item markings
              \item Differential and current probe supply.
          \end{itemize}
    \item Function Generator:

          Frequency generator CH1 and CH2 connected to DUT driver (HS/LS interchangeable).
          Trigger frequency generator connected to Oscilloscope CH1.
          Settings (One 5V pulse to turn on both GaN HEMTs):
          \begin{itemize}
              \item CH1/CH2 > Output Load > high Z.
              \item Unit = period. Period = tcal.
              \item CH1 and CH2: Duty Cycle = 99.9% ,not inverted.
              \item Amplitude 5Vpp; Offset 2.5V
              \item Burst CH1 and CH2: Cycle count 1; no start phase
          \end{itemize}
    \item Oscilloscope probes:

    \item load/drain current and RON Voltage drop
          \begin{itemize}
              \item Trigger: Ch1, 1.5 V rising edge.
              \item Ch1: Trigger-Signal, 1~M$\Omega$, 1 V/div, 3 V offset.
              \item Ch2: VTS(Temperature sensor voltage), 600 mV/div, 2.3 V offset.
              \item Ch3: Vcal (Vds,hs +Vds,ls), 300 mV/div, 1.2 V offset
              \item Ch4: Ical (Id,hs/Id,ls), 1 A/div, 3.5 A offset
              \item Sample rate 5k/s
          \end{itemize}

          \begin{itemize}
              \item c2 = -7.85 V/K; c1 = 58.55 K
              \item Function f1 (hide): Math, Multiply, CH2(Sensor)*Constant(-7.85)
              \item Function f2 (hide): Math, Add, f1+Constant(58.55)
              \item Function f3 (show): Filter, Smoothing, f2, 25 smoothing points
          \end{itemize}
    \item Oscilloscope power math function f4 (show): Math, Multiply, CH3(Vcal)*CH4(Ical)


\end{enumerate}


\subsubsection{Measurement execution}


In order to understand the thermal behaviour of the setup, the thermal step response for different conduction losses is investigated.
\begin{enumerate}
    \item Note the temperature sensor voltage at ambient (multimeter). Between measurements, cool the setup down to ambient utilizing the fan.
    \item Measure the thermal step response for Ical = 2 A and 4 A with tcal = 60 s. Utilize oscilloscope memory functions to compare them. Calculate the thermal impedance with an oscilloscope math-function

          \begin{itemize}
              \item Turn on supply 12V. Turn on 2.5V DC-Link voltage.
              \item Time-Settings: 6.2 s/div, 31s offset
              \item Trigger measurement with a frequency generator.
              \item Check measured voltage and current and turn on math functions for temperature and power.
              \item Use memory functions to save temperature and power and conduct the second measurement.
              \item Calculate thermal Impedance in Function f6. Scale: 2 K/W/div, Offset: 5 K
          \end{itemize}
    \item Measure the calibration curve $\Delta T_{sensor(Pcon,avg)} $ for tcal = 10 s and Ical = [1 2 3 4 5 6] A and plot the results in the figure.
          \begin{itemize}
              \item Turn off memory functions and Zth function.
              \item Time-Settings: 1.05 s/div, 5.25 s offset (to account for smoothing timespan)
              \item Set frequency generator duration to 11 s for both channels
              \item Add Markers (track Temp) for Temperature drop measurement.
              \item Add average power measurement marker at 0.1s and 9.9 s
              \item Set Ical as a current limit in supply CH2
              \item Cool your setup between measurements.
          \end{itemize}
    \item Calculate the thermal impedance $Z_{th,TS-amb}$(tcal = 10~s) based on your measurement results (linear approximation).
\end{enumerate}


%============================================================================================
% SUBSECTION
%============================================================================================
\subsection{Pulsed loss measurement}




In the second part of the experiment, the losses are measured calorimetrically for pulsed continuous operation. With an inductive-capacitive load, continuous operation with triangular
current is set. By specific selection of the inductance, the switching conditions (Vsw, Isw) and
the rms current and thus the conduction losses can be kept approximately constant for any
frequencies. The switching behaviour (hard/soft switching) is controlled via the dead time.


Driver Signals:

To generate a symmetrical triangular current at the frequency fsw over the measuring time tcal, high side and low side are controlled with 50 percent duty cycle in burst mode. In addition, the dead time must be adjustable. The switching frequency results from the load inductance L and the desired switching current $I_\Delta$ according to $f_{sw} = \frac{V_{DC} }{8L \hat I_{\Delta}}$. To meet the target dead time $t_{dead}$, the high side and low side must be controlled with a duty cycle slightly deviating from
50 percent and with a phase shift. Frequency and the calibration duration of tcal = 10 s gives the number of pulses/cycles. Table I provides the settings for different operating points.

Function generator general settings:
\begin{itemize}

    \item High output impedance (CH1/CH2 > Output Load > high Z)

    \item Square waveform

    \item Burst mode

    \item CH2: Polarity inverted

    \item High: 5V; Low: 0V

\end{itemize}


\subsubsection{Experiment Preparation}


Setup change:
\begin{enumerate}
    \item Turn off all supply voltages.
    \item Disconnect DC-Link Supply (CH2) and the current probe.
    \item Connect the Load-PCB.
    \item Connect the Coil.
    \item Connect HV-DC-Link-Supply. Check the markings on the DUT for polarity. Polarity is DUT dependent.
    \item Oscilloscope

          A. Change High-Voltage Probe to switch node voltage measurement

          B. Turn off power and Zth math function

          C. Turn on CH3 and CH4.

          D. CH3: scale 55 V/div and 210 V offset

          E. CH4: scale 1 A/div and 0 A offset

    \item Frequency generator:

          A. Burst 1000 pulse

          B. Unit freq

          C. Polarity CH2 inverted

          D. Set Frequency, D1, D2 and Phase according to table

\end{enumerate}


\subsubsection {Measurement Execution}


\begin{enumerate}
    \item Measure 1000 cycles at 400V, 3A, 200uH for the dead times 10, 40, 60 and 300 ns.

          \begin{itemize}
              \item Zoom in at a single period at 5 ms offset
              \item Compare the different waveforms, what can be observed for the different dead times?
          \end{itemize}
    \item In order to check how long the temperature sensor needs to settle after electromagnetic
          interference EMI due to pulsed operation zoom out to 2 ms/div and 10 ms offset. How
          large is the response time of the sensor?
          \begin{itemize}
              \item Investigate settle time with scaling 2 ms/div and 10 ms offset
          \end{itemize}
    \item Measure $\Delta T_{sensor}$ for tcal = 10~s of puled operation at 400 V, 3 A, 40 ns, 200uH.
          \begin{itemize}
              \item Set oscilloscope time scaling 1.1 s/div and 5 s offset
              \item Set marker to t1 = -100 ms and t2 = 10.01 s
              \item Set frequency generator burst to pulses from table.
          \end{itemize}
    \item Calculate the losses of this operating point with the measurement results and the calibration curve from part 1 and plot them in the figure
    \item Measure the losses for further dead times and add them to the figure.
    \item Compare the electrical measurement with your calorimetric results. What different loss phases are visible?
    \item Repeat the measurements for a second frequency and calculate the switching energy in optimal soft-switching operation. In order to achieve the same conduction losses with $I_{\Delta ,rms} = \frac{I_{\Delta}} {\sqrt{3}}$ you need to adjust the load. Turn off the voltage before changing the setup.

\end{enumerate}


%============================================================================================
% SECTION
%============================================================================================
\section {Results}


%============================================================================================
% SUBSECTION
%============================================================================================
\subsection {Measurement Calibration}
\label{sub:E2_measurement_calibration}


a) Why are the conduction losses not (always) constant? How could you prevent this?

\hspace{1cm}Conduction loss depends on the ON resistance of the transistor. Transistors have a PTC, where its resistance increases with increase in temperature. As a result, conduction loss increase as well. This can be prevented by providing sufficient cooling to the transistor during relaxation period (between tests).

b) What is the benefit of a transient calorimetric measurement compared to static?


\hspace{1cm}Although static calorimetric measurements can give precise data compared to transient one, the measurement technique is slow as we have to wait for the steady state to reach. The transient calorimetric measurement can capture rapid thermal changes and power dissipation in real-time, providing immediate feedback. It also measures the short-duration events like power spikes and switching losses effectively which will be missed by static methods. The short measurement times can also enhance testing and development cycles.

\Cref{tab:E2_Zth} shows the variation of $\Delta$T with respect to power loss for different currents.
$Z_{th}$ is found out as 1.635~$\Omega$. It is found by finding the slope of $\Delta T$ over power loss (\Cref{fig:E2_Zth}, \cref{eqn:Zth}).

\begin{equation}
    \begin{split}
        Z_{th} & = \frac{\Delta t_2 - \Delta t_1}{P_2 - P_1} \\
               & = \frac{4.88-0.103}{2.96-0.04}              \\
               & = 1.635 \Omega
    \end{split}
    \label{eqn:Zth}
\end{equation}


\begin{table}
    \centering
    \begin{tabular}{llll}
        \toprule
        I [A] & $\Delta$T [mK] & P [mW] \\
        \midrule
        1     & 103            & 40     \\
        2     & 455            & 224    \\
        4     & 1940           & 1120   \\
        6     & 4880           & 2960   \\
        \bottomrule
    \end{tabular}
    \caption{$\Delta T$ vs Power loss for different currents}
    \label{tab:E2_Zth}
\end{table}

%============================================================================================
% SUBSECTION
%============================================================================================
\subsection {Pulsed loss measurement}



\begin{figure}
    \centering
    \includegraphics[width=.8\textwidth]{figures/E2_dT.eps}
    \caption{Thermal impedance}
    \label{fig:E2_Zth}
\end{figure}
The power loss is calculated by dividing $\Delta T$ by $Z_{th}$ (calculated in \cref{sub:E2_measurement_calibration}) for different deadtimes (\Cref{tab:E2_P}). The graph between loss power and dead time is plotted in \cref{fig:E2_deadtime}. At very low dead times, the power loss is high, and it decreases gradually to an optimal dead time. Further increase in dead time lead to increase in power loss. The optimum dead time is found to be 100~ns for this setup. This is because with very low deadtime, hard switching occurs leading to higher losses. 
When both switches are off, the inductor current goes through the body diode, increasing losses~\cite{hanDeadTimeEffectGaNBased2015}.
Therefore, unnecessary high deadtime above optimum will again lead to higher losses. 


\begin{figure}
    \centering
    \includegraphics{figures/E2_deadtime.eps}
    \caption{Switching power loss with increasing dead time}
    \label{fig:E2_deadtime}
\end{figure}



\begin{table}
    \centering
    \begin{tabular}{lll}
        \toprule
        $t_{dead}$ [ns] & $\Delta$T [mK] & P [mW]       \\
        \midrule
        10              & 3980           & 2430         \\
        40              & 860            & 520          \\
        60              & 340            & 207          \\
        \textbf{100}    & \textbf{320}   & \textbf{195} \\
        200             & 560            & 340          \\
        300             & 720            & 440          \\
        \bottomrule
    \end{tabular}
    \caption{Power loss for different deadtimes}
    \label{tab:E2_P}
\end{table}

