%============================================================================================
% CHAPTER
%============================================================================================
\chapter{Dynamic Switching Voltage Measurements}


%============================================================================================
% SECTION
%============================================================================================
\section{Aim of the experiment}


The Aim of the experiment is to measure the switch node voltage of a GaN-based DC-DC converter. The typical switching characteristics (rise-time, overshoot) are to be determined. The distinction between real switching characteristics and measuring artefacts is also made and the bandwidth requirement for the probes and the oscilloscope is analysed. Furthermore, measurement of the voltage across the gate resistor and determination of gate charge of high/low-side transistor
is also carried out.


%============================================================================================
% SECTION
%============================================================================================
\section{Preparatory questions}


\begin{enumerate}
    \item Name three advantages a GaN transistor as compared to a Si MOSFET.
          \begin{itemize}
              \item High switching frequency → High power density and miniaturization capabilities
              \item High critical electric field → low internal resistance.
              \item Excellent thermal conductivity → can work at higher temperatures.
          \end{itemize}
    \item What are the advantages and disadvantages of a passive probe? Name three of each.
          \begin{itemize}
              \item[] \textbf{Advantages:}
              \item High dynamic range and high input impedance at low to medium frequencies.
              \item Rugged.
              \item They are cheap and don't require a power source
          \end{itemize}
          \begin{itemize}
              \item[] \textbf{Disadvantages:}
              \item Input impedance and the performance reduces at higher frequencies.
              \item Probe is grounded through the oscilloscope, which can lead to short circuits.
              \item Higher loading can affect the amplitude of the measured signal.
          \end{itemize}
    \item Why is it important to measure in the >500MHz range on a passive probe with the spring clip instead of using the ground cable?
          \begin{enumerate}
              \item[] The shorter spring clip has a lower inductance (length of ground path is reduced compared to the ground cable) which improves the bandwidth. This helps in improving accuracy at high frequencies.
          \end{enumerate}
    \item What is the maximum frequency at which the TI gate driver (LM5113) can be operated?
          \begin{enumerate}
              \item[] 5MHz.
          \end{enumerate}
    \item What does the abbreviation PWM stand for?
          \begin{enumerate}
              \item[] Pulse Width Modulation.
          \end{enumerate}
    \item How are the dead times of Low and High signal set during switching? Why are dead times important?
          \begin{enumerate}
              \item[] Dead times can be set with hardware (using RC circuits) and/or software (E.g., programming into firmware of the microcontroller). Dead times prevent shoot-through in half bridges where the supply is shorted by the highside and lowside switch. They can also reduce the switching losses by preventing hard switching. Deadtime should be high enough to prevent shoot through but excessive deadtime can lead to higher switching losses.
          \end{enumerate}
    \item What are the advantages and disadvantages of a differential probe? Name three each.
          \begin{itemize}
              \item[] \textbf{Advantages:}
              \item Measurement input does not have to be referenced to the oscilloscope ground.
              \item Good CMRR.
              \item Wide bandwidth with low input capacitance.
          \end{itemize}
          \begin{itemize}
              \item[] \textbf{Disadvantages:}
              \item High cost.
              \item Less rugged.
              \item Lower dynamic range.
          \end{itemize}
    \item Compare the equivalent circuit diagrams of a passive and differential probe. What are the differences?
          \begin{enumerate}
              \item[] In the passive probe, the tip is referenced to the ground while in the differential probe the input is fed into an amplifier circuit and is not referenced to ground. The passive probe also needs a compensation circuit which is not required in the differential probe. Also, the differential probe requires its own power source whereas the passive probes do not.
          \end{enumerate}
    \item Why is a probe with a particularly high common mode rejection CMRR required for gate charge measurement?
          \begin{enumerate}
              \item[] At higher frequencies, the mismatches in the probe become increasingly harder to control and the CMRR specification degrades, which might lead to measurement error as the slew rate of the probe decreases.
          \end{enumerate}
    \item What is the CMRR of the Tektronix IsoVu probe at 100MHz and 1GHz?
          \begin{enumerate}
              \item[] The CMRR is up to 120dB at 100MHz and up to 80dB at 1GHz.
          \end{enumerate}
\end{enumerate}

%============================================================================================
% SECTION
%============================================================================================
\section{Measurement Procedure}

%============================================================================================
% SUBSECTION
%============================================================================================
\subsection{Measurement of the voltage at the switching node with different probes}


\subsubsection{Experiment Preparation}


\begin{figure}
    \centering
    \includegraphics[width=.8\textwidth]{figures/E1_setup.png}
    \caption{Measurement setup}
    \label{fig:E1_setup}
\end{figure}

\begin{itemize}
    \item Check wire connections (see Illustration experiment setup figure \ref{fig:E1_setup}).
    \item Set DC source - channel 1 for "Driver supply" and switch it on.
    \item Check/execute function generator setting and switch it on.
    \item Set the DC source - channel 2 for "DC supply" but do not switch on yet.
\end{itemize}


\subsubsection{Measuring execution}


\begin{enumerate}
    \item Check that the function generator + DC source "driver / channel 1" are switched on.
    \item Check that the DC source power section / channel 2 is switched off.
    \item First, use the differential probe from BumbleBee.
    \item Start oscilloscope: Run mode.
    \item Enable the oscilloscope channel of the probe.
    \item Activate big button with the channel number (1/2/3)
    \item Push Trigger Source for the channel (1/2/3).
    \item Position the probe on board (at the switch node)
    \item Switch on DC source Power section / channel 2.
    \item Stop the oscilloscope.
    \item Switch off DC source power section / channel 2.
    \item Only now move / remove the probe.
    \item Extract the characteristic values.
    \item Enter the values in \cref{tab:E1_probe_comparison}.
    \item Tip: Use QuickMeas on oscilloscope to display the values to read.
    \item Repeat steps 3-14 for the other three probes
\end{enumerate}

%============================================================================================
% SUBSECTION
%============================================================================================
\subsection{Measurement of the gate charge}


\subsubsection{Experiment Preparation}


\begin{enumerate}
    \item Check wire connections (see \cref{fig:E1_setup}).
    \item Set DC source - channel 1 for Driver supply and switch it on.
    \item Check/execute function generator setting and switch it on.
    \item Set the DC source - channel 2 for DC supply but do not switch on yet.
\end{enumerate}


\subsubsection {Measurement Execution}


\begin{enumerate}
    \item Perform the measurement at a voltage of 15V, 20V and 30V for the Highside switch.
    \item Check that the function generator + DC source "driver / channel 1" are switched on.
    \item Check that the DC source power section / channel 2 is switched off.
    \item Start oscilloscope: Run.
    \item Enable the oscilloscope channel of the probe.
    \item Activate big button with the channel number (1/2/3).
    \item Push Trigger at Source for the channel (1/2/3).
    \item Check whether the probe is terminated with $50\Omega$.
    \item Switch on DC source Power section / channel 2.
    \item Extracting characteristic values.
          \begin{itemize}
              \item Maths function: Integral (integrate voltage curve at the gate).
              \item Use the cursors (a) and (b) to select a time range of 8ns.
              \item  Write down value / take screenshot.
          \end{itemize}
    \item Repeat procedure for the Lowside and other gate load.
\end{enumerate}


%============================================================================================
% SECTION
%============================================================================================
\section {Results}


%============================================================================================
% SUBSECTION 
%============================================================================================
\subsection {Measurement Evaluation for Part 1}


Exemplary measurement evaluation in \cref{tab:E1_probe_comparison}:

a) $t_{R}$ / $t_{F}$  : Rise / fall time, 10\% - 90\% in ns.

b) $V_{pk+}$ / $V_{pk-}$: Peak Overshoot / undershoot (max/min) in Voltage

c) Ringing quality : Qualitative description of the visible Oscillations after the switching process to compare the probe behaviour with the other probes by assigning them into low, medium or high ringing quality (low referring to a lot of ringing).

\subsubsection{Different measurement results}

The voltage behaviour is measured using the following probes - PMK BumbleBee,
TPP1000 [Spring Clip], PML791-PRO, P2000A. (\cref{fig:E1_1_bumblebee,fig:E1_1_spring,fig:E1_1_hvProbe,fig:E1_1_lowZ})


The fall times of all probes are nearly the same except the bumblebee, while the rise time remains almost the same for all probes (a little worse in TPP1000 [Spring Clip]). With respect to ringing quality and overshoot, BumbleBee shows the worst ringing and high overshoots. This is due to the fact the Bumblebee is a differential probe, which is more suitable for high frequency applications and in our scenario, the frequency is only 1~MHz and this probe has lower dynamic range than passive probes.  PML791-PRO and PML791-PRO shows minimum ringing. All probes other than Bumblebee shows moderate overshoots.


\subsubsection{Load probe}

A high impedance probe will generate less power dissipation internally whereas a low impedance will have a higher power loss and thus distort the measured voltage more and cannot measure high voltages. But still, the low impedance probe has higher bandwidth. In our case, Bumblebee has low impedance which leads to higher losses but has the benefit of higher bandwidth and the passive probes possess the opposite.


\begin{table}[!ht]

    \begin{center}

        \begin{tabular}{p{40mm}llllc}
            \toprule
            \textbf{Probe}        & $\mathbf{t_{R}}$ \textbf{[ns]} & $\mathbf{V_{pk+}}$ \textbf{[v]} & $\mathbf{t_{F}}$\textbf{[ns]} & $\mathbf{V_{pk-}}$\textbf{[v]} & \textbf{Ringing Quality} \\
            \midrule
            PMK BumbleBee         & 7.12                           & 14.09                           & 6.39                          & 30.6                           & Low                      \\
            TPP1000 [Spring Clip] & 10                             & 5.72                            & 15                            & 7.56                           & Medium                   \\
            PML791-PRO            & 7.12                           & 6.65                            & 14.8                          & 5.9                            & High                     \\
            P2000A                & 6.83                           & 6.83                            & 16.1                          & 5.08                           & High                     \\
            \bottomrule
        \end{tabular}
        \caption{Probe Performance Comparison}
        \label{tab:E1_probe_comparison}
    \end{center}
\end{table}


\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_1_bumblebee_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_1_bumblebee_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{Waveform captured with the bumblebee probe}
    \label{fig:E1_1_bumblebee}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_1_spring_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_1_spring_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{Waveform captured with the TPP1000 probe with spring clip}
    \label{fig:E1_1_spring}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_1_hvProbe_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_1_hvProbe_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{Waveform captured with the P2000A probe}
    \label{fig:E1_1_hvProbe}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_1_lowZ_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_1_lowZ_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{Waveform captured with the PML791-PRO probe}
    \label{fig:E1_1_lowZ}
\end{figure}


%============================================================================================
% SUBSECTION
%============================================================================================
\subsection {Measurement Evaluation for Part 2}

Gate charge of the MOSFET needs to evaluated as it directly impacts the switching speed by having an influence on Gate capacitance and is also important for selecting appropriate gate drivers. Low gate charge facilitates low switching loss.

In this part of the experiment, the gate charges of High side and Low side transistors at various voltages (15V, 20V and 30V) for 2 DUTs were calculated. This is done by integrating the voltage curve at the gate with respect time range of 80ns (till gate voltage curves oscillates).

The DUTs have different voltage ratings, which lead to different gate oxide thickness, device areas, gate capacitances leading to different gate charges. So the DUT with high voltage rating - DUT 2, has higher gate charge compared to the DUT with low voltage rating MOSFETs - DUT 1. \Cref{tab:E1_2_gate_charge_DUT1_HS,tab:E1_2_gate_charge_DUT1_LS,tab:E1_2_gate_charge_DUT2_LS,tab:E1_2_gate_charge_DUT2_HS} and \crefrange{fig:E1_2_E1DUT1HS15V}{fig:E1_2_E1DUT2LS30V} show the comparison of these gate charges.


\begin{table}[!ht]
    \centering

    \begin{tabular}{ccc}
        \toprule
        \textbf{Voltage [V]} & \textbf{Q\textsubscript{g,rise} [nC]} & \textbf{Q\textsubscript{g,fall}  [nC]} \\
        \midrule
        15                   & 20.2                                  & 21.8                                   \\
        20                   & 20.4                                  & 22.1                                   \\
        30                   & 20.6                                  & 23.4                                   \\
        \bottomrule
    \end{tabular}
    \caption{Comparison of Gate charge at various voltages for DUT1 High side}
    \label{tab:E1_2_gate_charge_DUT1_HS}
\end{table}


\begin{table}[!ht]
    \centering

    \begin{tabular}{ccc}
        \toprule
        \textbf{Voltage [V]} & \textbf{Q\textsubscript{g,rise} [nC]} & \textbf{Q\textsubscript{g,fall}  [nC]} \\
        \midrule
        15                   & 20.8                                  & 21.8                                   \\
        20                   & 20.7                                  & 22                                     \\
        30                   & 20.4                                  & 22.2                                   \\
        \bottomrule
    \end{tabular}
    \caption{Comparison of Gate charge at various voltages for DUT1 Low side}
    \label{tab:E1_2_gate_charge_DUT1_LS}
\end{table}



\begin{table}[!ht]
    \centering

    \begin{tabular}{ccc}
        \toprule
        \textbf{Voltage [V]} & \textbf{Q\textsubscript{g,rise} [nC]} & \textbf{Q\textsubscript{g,fall}  [nC]} \\
        \midrule
        15                   & 76.85                                 & 77.73                                  \\
        20                   & 76.91                                 & 78.83                                  \\
        30                   & 79.89                                 & 77.63                                  \\
        \bottomrule
    \end{tabular}
    \caption{Comparison of Gate charge at various voltages for DUT2 Low side}
    \label{tab:E1_2_gate_charge_DUT2_LS}
\end{table}


\begin{table}[!ht]
    \centering

    \begin{tabular}{ccc}
        \toprule
        \textbf{Voltage [V]} & \textbf{Q\textsubscript{g,rise} [nC]} & \textbf{Q\textsubscript{g,fall}  [nC]} \\
        \midrule
        15                   & 83.38                                 & 77.72                                  \\
        20                   & 83.17                                 & 78.95                                  \\
        30                   & 85.24                                 & 80.05                                  \\
        \bottomrule
    \end{tabular}
    \caption{Comparison of Gate charge at various voltages for DUT2 High side}
    \label{tab:E1_2_gate_charge_DUT2_HS}
\end{table}

%==============================
% DUT1
%==============================

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1HS15V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1HS15V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT1 HS falling edge 15V gate charge}
    \label{fig:E1_2_E1DUT1HS15V}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1LS15V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1LS15V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT1 LS falling edge 15V gate charge}
    \label{fig:E1_2_E1DUT1LS15V}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1HS20V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1HS20V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT1 HS falling edge 20V gate charge}
    \label{fig:E1_2_E1DUT1HS20V}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1LS20V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1LS20V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT1 LS falling edge 20V gate charge}
    \label{fig:E1_2_E1DUT1LS20V}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1HS30V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1HS30V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT1 HS falling edge 30V gate charge}
    \label{fig:E1_2_E1DUT1HS30V}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1LS30V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT1LS30V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT1 LS falling edge 30V gate charge}
    \label{fig:E1_2_E1DUT1LS30V}
\end{figure}
%==============================
% DUT2
%==============================
\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2HS15V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2HS15V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT2 HS falling edge 15V gate charge}
    \label{fig:E1_2_E1DUT2HS15V}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2LS15V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2LS15V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT2 LS falling edge 15V gate charge}
    \label{fig:E1_2_E1DUT2LS15V}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2HS20V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2HS20V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT2 HS falling edge 20V gate charge}
    \label{fig:E1_2_E1DUT2HS20V}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2LS20V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2LS20V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT2 LS falling edge 20V gate charge}
    \label{fig:E1_2_E1DUT2LS20V}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2HS30V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2HS30V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT2 HS falling edge 30V gate charge}
    \label{fig:E1_2_E1DUT2HS30V}
\end{figure}

\begin{figure}[!ht]
    \centering
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2LS30V_rise.eps}
        \caption{Rising edge}
    \end{subcaptionblock}%
    \begin{subcaptionblock}{.5\textwidth}
        \centering
        \includegraphics[width=\textwidth]{figures/E1_2_E1DUT2LS30V_fall.eps}
        \caption{Falling edge}
    \end{subcaptionblock}%
    \caption{DUT2 LS falling edge 30V gate charge}
    \label{fig:E1_2_E1DUT2LS30V}
\end{figure}


